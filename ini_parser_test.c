static char * g_szIniString = "#abc\nfirst=2\nsecond\nname=charli  zhang \n";  
  
static void ini_parser_test_string()  
{  
    struct ini_parser * ini = new_ini_parser();  
    int size = ini->parse_string(ini, g_szIniString);  
  
    assert( size > 0);  
    assert( ini->value(ini, "second") == 0 );  
    assert( ini->value(ini, "abc") == 0);  
    assert( ini->value(ini, "name") != NULL );  
    assert( ini->value(ini, "first") != NULL);  
  
    printf("ini string: %s\n", g_szIniString);  
    printf("key-value pairs count = %d\n", size);  
    printf("key \'name\'', value = %s\n", ini->value(ini, "name"));  
    printf("key \'first\'', value = %s\n", ini->value(ini, "first"));  
  
    ini->set_value(ini, "baidu", "hahaha");  
    ini->save_to_file(ini, "write.conf");  
  
    ini->remove(ini, "first");  
    ini->save_to_file(ini, "write2.conf");  
  
    ini->deletor(ini);  
}  
  
static void ini_parser_test_file()  
{  
    struct ini_parser * ini = new_ini_parser();  
    int size = ini->parse_file(ini, "test.conf");  
  
    assert( size > 0);  
    assert( ini->value(ini, "second") == 0 );  
    assert( ini->value(ini, "abc") == 0);  
    assert( ini->value(ini, "name") != NULL );  
    assert( ini->value(ini, "first") != NULL);  
  
    printf("ini string: %s\n", g_szIniString);  
    printf("key-value pairs count = %d\n", size);  
    printf("key \'name\'', value = %s\n", ini->value(ini, "name"));  
    printf("key \'first\'', value = %s\n", ini->value(ini, "first"));  
    printf("key \'baidu\'', value = %s\n", ini->value(ini, "baidu"));  
  
    ini->deletor(ini);  
}  
  
void ini_parser_test()  
{  
    ini_parser_test_string();  
    ini_parser_test_file();  
}  