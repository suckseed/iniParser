/* call 1 or N arguments function of struct */  
#define ST_CALL(THIS,func,args...) ((THIS)->func(THIS,args))  
  
/* call none-arguments function of struct */  
#define ST_CALL_0(THIS,func) ((THIS)->func(THIS))  
  
struct int_node {  
    struct slist_node node;  
    int id;  
};  
  
struct string_node {  
    struct slist_node node;  
    char name[16];  
};  
  
  
static int int_free_flag = 0;  
static void _int_child_free(struct slist_node *node)  
{  
    free(node);  
    if(!int_free_flag)  
    {  
        int_free_flag = 1;  
        printf("int node free\n");  
    }  
}  
  
static int _int_slist_hittest(struct slist_node * node, void *key)  
{  
    struct int_node * inode = NODE_T(node, struct int_node);  
    int ikey = (int)key;  
    return (inode->id == ikey ? 0 : 1);  
}  
  
static int string_free_flag = 0;  
static void _string_child_free(struct slist_node *node)  
{  
    free(node);  
    if(!string_free_flag)  
    {  
        string_free_flag = 1;  
        printf("string node free\n");  
    }  
}  
  
static int _string_slist_hittest(struct slist_node * node, void *key)  
{  
    struct string_node * sn = (struct string_node*)node;  
    return strcmp(sn->name, (char*)key);  
}  
  
void int_slist_test()  
{  
    struct single_list * list = new_single_list(_int_child_free, _int_slist_hittest);  
    struct int_node * node = 0;  
    struct slist_node * bn = 0;  
    int i = 0;  
  
    printf("create list && nodes:\n");  
    for(; i < 100; i++)  
    {  
        node = (struct int_node*)malloc(sizeof(struct int_node));  
        node->id = i;  
        if(i%10)  
        {  
            list->add(list, node);  
        }  
        else  
        {  
            list->insert(list, 1, node);  
        }  
    }  
    printf("create 100 nodes end\n----\n");  
    printf("first is : %d, last is: %d\n----\n",  
           NODE_T( ST_CALL_0(list, first), struct int_node )->id,  
           NODE_T( ST_CALL_0(list, last ), struct int_node )->id);  
  
    assert(list->size == 100);  
  
    printf("list traverse:\n");  
    for(i = 0; i < 100; i++)  
    {  
        if(i%10 == 0) printf("\n");  
        bn = list->at(list, i);  
        node = NODE_T(bn, struct int_node);  
        printf(" %d", node->id);  
    }  
    printf("\n-----\n");  
  
    printf("find by key test, key=42:\n");  
    bn = list->find_by_key(list, (void*)42);  
    assert(bn != 0);  
    node = NODE_T(bn, struct int_node);  
    printf("find node(key=42), %d\n------\n", node->id);  
  
    printf("remove node test, remove the 10th node:\n");  
    bn = list->at(list, 10);  
    node = NODE_T(bn, struct int_node);  
    printf("  node 10 is: %d\n", node->id);  
    printf("  now remove node 10\n");  
    list->remove_at(list, 10);  
    printf(" node 10 was removed, check node 10 again:\n");  
    bn = list->at(list, 10);  
    node = NODE_T(bn, struct int_node);  
    printf("  now node 10 is: %d\n------\n", node->id);  
  
    printf("replace test, replace node 12 with id 1200:\n");  
    bn = list->at(list, 12);  
    node = NODE_T(bn, struct int_node);  
    printf("  now node 12 is : %d\n", node->id);  
    node = (struct int_node*)malloc(sizeof(struct int_node));  
    node->id = 1200;  
    list->replace(list, 12, node);  
    bn = list->at(list, 12);  
    node = NODE_T(bn, struct int_node);  
    printf("  replaced, now node 12 is : %d\n----\n", node->id);  
  
    printf("test remove:\n");  
    ST_CALL(list, remove, bn);  
    bn = ST_CALL(list, find_by_key, (void*)1200);  
    assert(bn == 0);  
    printf("test remove ok\n----\n");  
    printf("test remove_by_key(90):\n");  
    ST_CALL(list, remove_by_key, (void*)90);  
    bn = ST_CALL(list, find_by_key, (void*)90);  
    assert(bn == 0);  
    printf("test remove_by_key(90) end\n----\n");  
    printf("test take_at(80):\n");  
    bn = ST_CALL(list, take_at, 80);  
    printf("  node 80 is: %d\n", NODE_T(bn, struct int_node)->id);  
    free(bn);  
    printf("test take_at(80) end\n");  
  
    int_free_flag = 0;  
    printf("delete list && nodes:\n");  
    list->deletor(list);  
    printf("delete list && nodes end\n");  
    printf("\n test add/insert/remove/delete/find_by_key/replace...\n");  
}  
  
void string_slist_test()  
{  
    struct single_list * list = new_single_list(_string_child_free, _string_slist_hittest);  
}  
  
void slist_test()  
{  
    int_slist_test();  
    string_slist_test();  
}  